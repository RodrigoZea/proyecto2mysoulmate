#Script para limpiar archivo de nodos...
#Francisco Molina -17050
#Rodrigo Zea - 17058 :)


#TODOS LOS METODOS RETORNAN UNA LISTA.
#el archivo a leer debe estar en mismo path.

#Para debug usar print(cleanName[0]) -> retorna lo que se obtiene del JSON.

#El offset de linea es calculado asi: Numero de linea anterior a la propiedad (targetLine)

#El offset de propiedad es calculado asi: 6 espacios para encontrar la propiedad. Numero de caracteres utilizados antes del valor de esa propiedad. El segundo - primero.

# Se comprobo que TODAS las propiedades impriman el numero de nodos.

#funcion para obtener nombres...

# -*- coding: utf-8 -*-
def getName():
    lista=[];
    targetFile= open("nodeInfo.txt",'r')
    targetLine= 7
    for i, line in enumerate(targetFile):
        if( i == targetLine):   
            x=line
        #print(x)
            posicionDelTag=x.find("name")
            # 7 posiciones despues esta el nombre.
            inicioNombre=posicionDelTag+8
            #print(inicioNombre)
            dirtyName=x[inicioNombre:]
            cleanName=dirtyName.split('"')
            #se agrega a una lista de atributos..
            lista.append(cleanName[0])
            targetLine=targetLine+16
    targetFile.close
    return lista

#funcion para obtener apellidos... 
def getLastName():
    lista=[];
    targetFile= open("nodeInfo.txt",'r')
    targetLine= 8
    for i, line in enumerate(targetFile):
        if( i == targetLine):   
            x=line
        #print(x)
            posicionDelTag=x.find("lastName")
            # 13 posiciones despues esta el apellido.
            inicioNombre=posicionDelTag+12
            #print(inicioNombre)
            dirtyName=x[inicioNombre:]
            cleanName=dirtyName.split('"')
            #se agrega a lista..
            lista.append(cleanName[0])
            targetLine=targetLine+16
    targetFile.close
    return lista

#obtiene propiedades fisicas.
def getPhysChar():
    lista=[];
    targetFile= open("nodeInfo.txt",'r')
    #offset donde inicia la propiedad del nodo.
    targetLine= 3
    for i, line in enumerate(targetFile):
        if( i == targetLine):   
            x=line
            posicionDelTag=x.find("PhysCharacteristics")
            # 23 posiciones despues esta la caracteristica.
            inicioNombre=posicionDelTag+23
            dirtyName=x[inicioNombre:]
            cleanName=dirtyName.split('"')
            #se agrega a la lista..
            lista.append(cleanName[0])
            #offset para el siguiente nodo.
            targetLine=targetLine+16
    targetFile.close
    return lista

def getPshyco():
    lista=[];
    targetFile= open("nodeInfo.txt",'r')
    #offset donde inicia la propiedad del nodo.
    targetLine= 4
    for i, line in enumerate(targetFile):
        if( i == targetLine):   
            x=line
            posicionDelTag=x.find("PshycoCharacteristics")
            # 25 posiciones despues esta la caracteristica.
            inicioNombre=posicionDelTag+25
            dirtyName=x[inicioNombre:]
            cleanName=dirtyName.split('"')
            # se agrega a la lista
            lista.append(cleanName[0])
            #offset para el siguiente nodo.
            targetLine=targetLine+16
    targetFile.close
    return lista

def getAge():
    lista=[];
    targetFile= open("nodeInfo.txt",'r')
    #offset donde inicia la propiedad del nodo.
    targetLine= 5
    for i, line in enumerate(targetFile):
        if( i == targetLine):   
            x=line
            posicionDelTag=x.find("age")
            # 22 posiciones despues esta la caracteristica.
            inicioNombre=posicionDelTag+6
            dirtyName=x[inicioNombre:]
            #aqui varia porque no hay "" hay ,
            cleanName=dirtyName.split(',')
            #se agrega a la lista
            lista.append(cleanName[0])
            #offset para el siguiente nodo.
            targetLine=targetLine+16
    targetFile.close
    return lista

def getEye():
    lista=[];
    targetFile= open("nodeInfo.txt",'r')
    #offset donde inicia la propiedad del nodo.
    targetLine= 6
    for i, line in enumerate(targetFile):
        if( i == targetLine):   
            x=line
            posicionDelTag=x.find("eyeColor")
            # 13 posiciones despues esta la caracteristica.
            inicioNombre=posicionDelTag+12
            dirtyName=x[inicioNombre:]
            cleanName=dirtyName.split('"')
            #se agrega a la lista
            lista.append(cleanName[0])
            #offset para el siguiente nodo.
            targetLine=targetLine+16
    targetFile.close
    return lista
    
def getPhone():
    lista=[];
    targetFile= open("nodeInfo.txt",'r')
    #offset donde inicia la propiedad del nodo.
    targetLine=11
    for i, line in enumerate(targetFile):
        if( i == targetLine):   
            x=line
            posicionDelTag=x.find("phone")
            # 10 posiciones despues esta la caracteristica.
            inicioNombre=posicionDelTag+9
            dirtyName=x[inicioNombre:]
            cleanName=dirtyName.split('"')
            #se agrega a lista
            lista.append(cleanName[0])
            #offset para el siguiente nodo.
            targetLine=targetLine+16
    targetFile.close
    return lista

def getFood():
    lista=[];
    targetFile= open("nodeInfo.txt",'r')
    #offset donde inicia la propiedad del nodo.
    targetLine= 12
    for i, line in enumerate(targetFile):
        if( i == targetLine):   
            x=line
            posicionDelTag=x.find("favoriteFood")
            # 22 posiciones despues esta la caracteristica.
            inicioNombre=posicionDelTag+16
            dirtyName=x[inicioNombre:]
            cleanName=dirtyName.split('"')
            #se agrega a la lista
            lista.append(cleanName[0])
            #offset para el siguiente nodo.
            targetLine=targetLine+16
    targetFile.close
    return lista

def getLikesPhys():
    lista=[];
    targetFile= open("nodeInfo.txt",'r')
    #offset donde inicia la propiedad del nodo.
    targetLine= 9
    for i, line in enumerate(targetFile):
        if( i == targetLine):   
            x=line
            posicionDelTag=x.find("likesPhys")
            # 13 posiciones despues esta la caracteristica.
            inicioNombre=posicionDelTag+13
            dirtyName=x[inicioNombre:]
            cleanName=dirtyName.split('"')
            #se agrega a la lista
            lista.append(cleanName[0])
            #offset para el siguiente nodo.
            targetLine=targetLine+16
    targetFile.close
    return lista

def getLikesPshyco():
    lista=[];
    targetFile= open("nodeInfo.txt",'r')
    #offset donde inicia la propiedad del nodo.
    targetLine= 10
    for i, line in enumerate(targetFile):
        if( i == targetLine):   
            x=line
            posicionDelTag=x.find("likesPshyco")
            # 22 posiciones despues esta la caracteristica.
            inicioNombre=posicionDelTag+15
            dirtyName=x[inicioNombre:]
            cleanName=dirtyName.split('"')
            #se agrega a la lista
            lista.append(cleanName[0])
            #offset para el siguiente nodo.
            targetLine=targetLine+16
    targetFile.close
    return lista

def getSex():
    lista = []
    targetFile= open("nodeInfo.txt",'r')
    #offset donde inicia la propiedad del nodo.
    targetLine= 13
    for i, line in enumerate(targetFile):
        if( i == targetLine):   
            x=line
            posicionDelTag=x.find("sex")
            # 22 posiciones despues esta la caracteristica.
            inicioNombre=posicionDelTag+7
            dirtyName=x[inicioNombre:]
            cleanName=dirtyName.split('"')
            #se agrega a la lista
            lista.append(cleanName[0])
            #offset para el siguiente nodo.
            targetLine=targetLine+16
    targetFile.close
    return lista

def getUsername():
    lista = []
    targetFile= open("nodeInfo.txt",'r')
    #offset donde inicia la propiedad del nodo.
    targetLine= 14
    for i, line in enumerate(targetFile):
        if( i == targetLine):   
            x=line
            posicionDelTag=x.find("username")
            # 22 posiciones despues esta la caracteristica.
            inicioNombre=posicionDelTag+12
            dirtyName=x[inicioNombre:]
            cleanName=dirtyName.split('"')
            #se agrega a la lista
            lista.append(cleanName[0])
            #offset para el siguiente nodo.
            targetLine=targetLine+16
    targetFile.close
    return lista

def getPass():
    lista = []
    targetFile= open("nodeInfo.txt",'r')
    #offset donde inicia la propiedad del nodo.
    targetLine= 15
    for i, line in enumerate(targetFile):
        if( i == targetLine):   
            x=line
            posicionDelTag=x.find("pw")
            # 22 posiciones despues esta la caracteristica.
            inicioNombre=posicionDelTag+6
            dirtyName=x[inicioNombre:]
            cleanName=dirtyName.split('"')
            #se agrega a la lista
            lista.append(cleanName[0])
            #offset para el siguiente nodo.
            targetLine=targetLine+16
    targetFile.close
    return lista
