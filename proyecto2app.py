"""
Universidad del Valle de Guatemala
Algoritmos y Estructuras de Datos
            Seccion 20
----------- PROYECTO 2 -----------
            MySoulmate
    Un sistema de recomendacion
    
      INTEGRANTES DEL EQUIPO
     Francisco Molina - 17050
     Rodrigo Zea - 17058
     Rodrigo Urrutia - 16139
"""
# -*- coding: utf-8 -*-
from neo4jrestclient.client import GraphDatabase
from neo4jrestclient import client
from cleaningScript import *
import sys

# Variable que guarda el nombre del usuario en sesion
InNombre="a"

# Variable loop infinito
j = 0

# Conexion a la base de datos de grafos
db = GraphDatabase("http://localhost:7474", username="neo4j", password="mypassword")

# Creacion de labels
usuarios = db.labels.create("Usuarios")
#cualidades = db.labels.create("Cualidades")
#gustos = db.labels.create("Gustos")

# Solo utilizar cuando se quieren agregar los nodos. 
def add_nodes():
    # Lista de referencia para el for. Deberia tener 100 items
    lista_users = getName()    

    nombre = getName()
    apellido = getLastName()
    physC = getPhysChar()
    pshycoC = getPshyco()
    edad = getAge()
    ojo = getEye()
    celular = getPhone()
    comida = getFood()
    gustosFisicos = getLikesPhys()
    gustosActitud = getLikesPshyco()
    genero = getSex()
    usuario = getUsername()
    passw = getPass()
    i=0
    #en la creacion de nodos, pw es el password.. el unico que tendra password es quien use el programa.
    for r in lista_users:
        persona = db.nodes.create(username = usuario[i], name=nombre[i], last=apellido[i], phys=physC[i], pshyco=pshycoC[i]
                                  , age=edad[i], eye=ojo[i], phone=celular[i], food=comida[i], pw=passw[i]
                                  , likesPhys =gustosFisicos[i], likesPshyco=gustosActitud[i], sex=genero[i], descripcion="", favSong ="")
        usuarios.add(persona)
        i += 1

#COMENTAR Y DESCOMENTAR PARA LLENAR LA BASE DE DATOS
#add_nodes()

# Se imprimen las opciones que el usuario puede escoger
def print_menu():
    print(" ")
    print("--- Bienvenido a MySoulmate ---")
    print("--- Ingrese la opcion que desea tomar ---")
    print("1. Registrar usuario")
    print("2. Login usuario")
    print("3. Salir")
    print (' ------------------- ')
    print (' ')

def print_UserMenu():
    print(" ")
    print("--- Sesion Iniciada ---")
    print("--- Ingrese la opcion que desea tomar ---")
    print("1. Lista de Matches")
    print("2. Obtener recomendaciones")
    print("3. Denunciar usuario")
    print("4. Log Out")
    print (' ------------------- ')
    print (' ')

def show_list():
    print("Escoga personalmente de la siguiente lista cualidades que usted posee: ")
    print("¿Cuantos cualidades")     

#metodo que crea nodo para un usuario.
def register_user():
    # Ingreso de datos del usuario
    userName= input("Ingrese su nombre de usuario -debe ser unico- : ")
    uPass = input("Ingrese su contrasena: ")
    uSexo = input("Ingrese el sexo al que pertenece (male-Hombre | female-Mujer): ")
    uName = input("Ingrese su primer nombre: ")
    uLast = input("Ingrese su primer apellido: ")    
    uPhysC= input("Ingrese su caracteristica fisica mas representativa: ")
    uPshycoC= input("Ingrese su caracteristica de personalidad mas representativa: ")
    uEdad=input("Ingrese su edad: ")
    uOjo=input("Ingrese su color de ojos: ")
    uCel=input("Ingrese su numero celular: ")
    uCancion=input("Ingrese su cancion favorita: ")
    uComida=input("Ingrese su comida favorita: ")
    uDescripcion=input("De una breve descripcion personal: ")
    uLikesPhysC=input("Ingrese gusto fisico: ")
    uLikesPshyco= input("Ingrese gusto de actitud: ")
    
    #antes hagamos un query por sanidad mental *no debe existir usuario con ese ID*
    qComprobar='MATCH (u:Usuarios) WHERE u.username ="'+userName+'" RETURN u'
    result= db.query(qComprobar, returns=(client.Node))
    cantResult=len(result)
    if (cantResult !=0):
        print("Nombre de usuario ya registrado, intente con un nombre de usuario distinto")
        return
    else:
         #se agrega a db
        newUser=db.nodes.create( username=userName, sex = uSexo, descripcion=uDescripcion, name=uName,
                                 phys=uPhysC, pshyco=uPshycoC, age=uEdad, eye=uOjo, phone=uCel,
                                 favSong=uCancion, food=uComida, pw=uPass, last=uLast, likesPhys=uLikesPhysC, likesPshyco= uLikesPshyco)

        #se agrega local a python..
        usuarios.add(newUser)

        print("Registro realizado exitosamente. Por favor, inicie sesion \n")
        
#entra a la sesion de usuario.  
def login_user():
    #Se deberia corroborar desde db. Realizar queries de MATCH usuario y MATCH PW.
    passed=False
    global InNombre
    InNombre=input("Ingrese su nombre de usuario registrado: ")
    InPass= input("Ingrese su contraseña: ")
    qUserLog= 'MATCH (u:Usuarios) WHERE u.username ="'+InNombre+'" AND u.pw="'+ InPass +'"  RETURN u' #deberia ser solo 1 resultado.
    result= db.query(qUserLog, returns=(client.Node))
    totalMatch=len(result)
    if (totalMatch ==0):
        print("Datos invalidos.\n")
    else:
        passed= True
        print("Ha ingresado.\n")
        global j
        j=2

#este metodo ejecuta las recomendaciones.
def search_person():
    global InNombre

    q1='MATCH (u:Usuarios) WHERE u.username ="'+InNombre+'" RETURN u'
    q1Node= db.query(q1, returns=(client.Node))
    
    for r in q1Node:
        edad1= r[0]["age"]
        sexo1= r[0]["sex"]
        
        phys1 = r[0]["likesPhys"]
        psycho1 = r[0]["likesPshyco"]

        leq=int(int(edad1)/2+7)
        geq=2*(int(edad1)-7)
    print("---------------------------------------------------")
    print("Ingrese el genero de personas que quiere buscar")
    print("1. Hombre")
    print("2. Mujer")
    gender = input( "Ingrese el genero deseado (1/2): ")
    print("---------------------------------------------------")
    if (gender =="1"):
        gender = "male"
    if (gender == "2"):
        gender = "female"
    q2='MATCH (u:Usuarios) WHERE u.sex ="'+gender+'" AND (u.phys="'+phys1+'" OR u.pshyco="'+psycho1+'") RETURN u'
    q2Node= db.query(q2, returns=(client.Node))
    
    for r in q2Node:
        edad2= r[0]["age"]
        username2 = r[0]["username"]
        y=0 #variable verificadora
        
        q4='MATCH (u:Usuarios)-[n:likes]->(d:Usuarios {username:"'+username2+'"}) RETURN u'
        q4Node= db.query(q4, returns=(client.Node))
        #se buscan todas las personas que le gustan al q2Node
        for j in q4Node:
            nombrecheck = j[0]["username"]
            #si entre esas personas esta el usuario en sesion
            #entonces que no se despliegue la opcion de dar like porque ya le dio like
            if nombrecheck== InNombre:
                y=1

        #lo mismo se debe hacer en el caso de que la persona ya tenga match con otra

        q5='MATCH (u:Usuarios)-[n:match]->(d:Usuarios {username:"'+username2+'"}) RETURN u'
        q5Node= db.query(q5, returns=(client.Node))
        #se buscan todas las personas que le gustan al q2Node
        for j in q5Node:
            nombrecheck = j[0]["username"]
            #si entre esas personas esta el usuario en sesion
            #entonces que no se despliegue la opcion de dar like porque ya le dio like
            if nombrecheck== InNombre:
                y=1
                
        #si el usuario aun no le ha dado like, entonces que vea el perfil para decidir si darle o no
        if leq<= int(edad2) and int(edad2)<= geq and y==0:
            print(("\n" + "PERFIL SUGERIDO \n\n" + "Nombre: %s " % (r[0]["name"]))
                  + "\n" +("Apellido: %s" % (r[0]["last"]))
                  + "\n" +("Descripcion: %s" % (r[0]["descripcion"]))
                  + "\n" +("Caracteristica fisica representativa: %s" % (r[0]["phys"])) 
                  + "\n" +("Caracteristica de personalidad representativa: %s" % (r[0]["pshyco"])) 
                  + "\n" +("Edad: %s" % (r[0]["age"])) 
                  + "\n" +("Color de ojos: %s" % (r[0]["eye"])) 
                  + "\n" +("Cancion favorita: %s" % (r[0]["favSong"]))
                  + "\n" +("Comida favorita: %s" % (r[0]["food"])) + "\n")

            x = input('Le gusta el perfil? (0-No | 1-Si |2-Regresar): ')
            
            if int(x)==1:
                q3='MATCH (u:Usuarios) WHERE u.username ="'+InNombre+'" RETURN u'
                q3Node= db.query(q3, returns=(client.Node))
                for i in q3Node:
                    i[0].relationships.create("likes", r[0])

            if int(x)==2:
                #El usuario ha decidido dejar de buscar recomendaciones
                break 
                
    return

def exit_program():
    print(" ")
    print("Ha salido del programa y cerrado sesion.")
    sys.exit(0)
def log_out():
    print("")
    print("Se ha cerrado sesion.")
    global j
    j=0
def denunciarUser():
    #Denuncia al usuario en caso de "x" evento. Esto lo elimina de la DB.
    print ('----------------------------------------------------------------------------')
    print ("")
    print ('Lamentamos lo ocurrido, provea la informacion de la persona que lo agredio.')
    agresorID=input("Ingrese el nombre de el agresor: ")
    numCel= input("Ingrese el celular: ")

    qDelete ='MATCH (u:Usuarios) WHERE u.name ="'+ agresorID +'" AND u.phone="'+ numCel +'"  DETACH DELETE u' #deberia ser solo 1 resultado.
    qTronitos= db.query(qDelete, returns=(client.Node))
    print("Usuario eliminado y reportado.")
    print ("")
    print ('----------------------------------------------------------------------------')
    
def posibleMatch():
    #metodo que verifica si el usuario, al iniciar sesion, tiene solicitudes de like
    global InNombre
    q1='MATCH (u:Usuarios)-[n:likes]->(d:Usuarios {username:"'+InNombre+'"}) RETURN u'
    q1Node= db.query(q1, returns=(client.Node))
    for r in q1Node:
        # de primero se debe evaluar si el usuario ya le dio dislike a este perfil
        # de ser asi, solo se pasa al siguiente perfil
        q3='MATCH (u:Usuarios {username:"'+InNombre+'"})-[n:dislike]->(d:Usuarios) RETURN d'
        q3Node= db.query(q3, returns=(client.Node))
        z=0 #simple verificador
        for j in q3Node:
            if r[0]["username"]==j[0]["username"]:
                #si el usuario ya le dio dislike a la propuesta
                z=1

        # de la misma manera, se debe evaluar si el usuario ya hizo match con este perfil
        q4='MATCH (u:Usuarios {username:"'+InNombre+'"})-[n:match]->(d:Usuarios) RETURN d'
        q4Node= db.query(q4, returns=(client.Node))
        for j in q4Node:
            if r[0]["username"]==j[0]["username"]:
                #si el usuario ya hizo match
                z=1
                
        if z==1:
            break
        
        print(("\n" + "PERFIL SUGERIDO \n\n" + "Nombre: %s " % (r[0]["name"]))
                  + "\n" +("Apellido: %s" % (r[0]["last"]))
                  + "\n" +("Descripcion: %s" % (r[0]["descripcion"]))
                  + "\n" +("Caracteristica fisica representativa: %s" % (r[0]["phys"])) 
                  + "\n" +("Caracteristica de personalidad representativa: %s" % (r[0]["pshyco"])) 
                  + "\n" +("Edad: %s" % (r[0]["age"])) 
                  + "\n" +("Color de ojos: %s" % (r[0]["eye"])) 
                  + "\n" +("Cancion favorita: %s" % (r[0]["favSong"]))
                  + "\n" +("Comida favorita: %s" % (r[0]["food"])) + "\n")

        x = input('Le gusta el perfil? (0-No | 1-Si): ')

        if int(x)==1:
            #si el usuario acepta la propuesta, hay match 
            q2='MATCH (u:Usuarios) WHERE u.username ="'+InNombre+'" RETURN u'
            q2Node= db.query(q2, returns=(client.Node))
            #este es el nodo del usuario en sesion
            for i in q2Node:
                i[0].relationships.create("match", r[0])
                r[0].relationships.create("match", i[0])
            print("FELICIDADES. Podras encontrar el numero de telefono de tu match en tu menu \n")
        else:
            #si el usuario rechaza a la propuesta, le da dislike
            q2='MATCH (u:Usuarios) WHERE u.username ="'+InNombre+'" RETURN u'
            q2Node= db.query(q2, returns=(client.Node))
            #este es el nodo del usuario en sesion
            for i in q2Node:
                i[0].relationships.create("dislike", r[0])

    return

def huboMatch():
    #el usuario consulta con quien ha hecho match para desplegar informacion de contacto
    global InNombre
    q1='MATCH (u:Usuarios)-[n:match]->(d:Usuarios {username:"'+InNombre+'"}) RETURN u'
    q1Node= db.query(q1, returns=(client.Node))
    for r in q1Node:
        print(("\n" + "PERFIL SUGERIDO \n\n" + "Nombre: %s " % (r[0]["name"]))
                  + "\n" +("Apellido: %s" % (r[0]["last"]))
                  + "\n" +("Telefono: %s" % (r[0]["phone"]))+ "\n")
        
    return

# Opciones a tomar. Switch.
options = {'1': register_user, '2': login_user, '3': exit_program}

# Menu de usuario.
UserOptions = {'1': huboMatch, '2': search_person, '3': denunciarUser, '4': log_out }

# Metodo principal, main
# Bucle infinito de opciones
# add_nodes() - Descomentar cuando se necesite agregar nodos
while (j < 1):
    print_menu()
    opt = input('Ingrese la opcion que quiere tomar: ')
    options[opt]()
    
#menu de usuario.
while(j > 1):
    posibleMatch()
    print_UserMenu()
    option= input("Ingrese la opcion que quiere tomar: ")
    UserOptions[option]()
